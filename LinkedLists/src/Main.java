import java.util.Iterator;
import java.util.ListIterator;

public class Main {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.insertBack(1);
        list.insertBack(2);
        list.insertBack(3);
        list.insertBack(4);
        list.insertBack(4);
        list.insertBack(200);
        list.insertBack(4);
        list.insertBack(4);
        list.insertBack(5);
        list.insertBack(6);
        list.insertBack(7);
        list.insertFront(300);
        list.insertFront(200);
        list.insertFront(100);
        System.out.println("Print forward:");
        list.printListForward();
        System.out.println("Print backward:");
        list.printListReverse();
        list.removeFront();
        list.removeBack();
        System.out.println("\nRemoved front and back.\n");
        System.out.println("Print forward:");
        list.printListForward();
        System.out.println("Print backward:");
        list.printListReverse();
        list.removeAllInstancesOf(200);
        list.removeAllInstancesOf(4);
        list.removeAllInstancesOf(6);
        System.out.println("\nRemoved all instances of 4, 6, and 200.\n");
        System.out.println("Print forward:");
        list.printListForward();
        System.out.println("Print backward:");
        list.printListReverse();
        System.out.println("\nFront: " + list.getFirst());
        System.out.println("Back: " + list.getLast());
        list.clear();
        System.out.println("\nList is cleared.\n");
        System.out.println("Print forward:");
        list.printListForward();
        System.out.println("Print backward:");
        list.printListReverse();
    }
}















/**
 * The LinkedList class is a doubly linked list that has methods for traversing,
 * printing, inserting, and removing nodes.
 * 
 * @author Toni N. Tran
 * 
 * @param <T>
 *            The type of data that will be stored in the LinkedList.
 */
class LinkedList<T> {

    Node head, tail;



    /**
     * Default constructor for the LinkedList.
     */
    public LinkedList() {
        head = new Node();
        head.next = null;
        tail = null;
    }



    /**
     * Inserts a Node with the wrapped data in front of the list. It is placed
     * right after the head node. Adjust all references accordingly. Ignores
     * duplicates and always inserts.
     * 
     * @param toInsert
     *            The object to wrap and insert.
     */
    public void insertFront(T toInsert) {
        Node toAdd = new Node(toInsert);
        // If the list is empty.
        if (head.next == null) {
            toAdd.prev = head;
            head.next = toAdd;
        } else {
            // Add the node in front of the head node and adjust references.
            toAdd.prev = head;
            toAdd.next = head.next;
            head.next.prev = toAdd;
            head.next = toAdd;
        }
    }



    /**
     * Inserts a Node with the wrapped data at the end of the list. It is placed
     * right after the tail node. Adjust all references accordingly. Ignores
     * duplicates and always inserts.
     * 
     * @param toInsert
     *            The object to wrap and insert.
     */
    public void insertBack(T toInsert) {
        Node toAdd = new Node(toInsert);
        // If the list is empty.
        if (tail == null) {
            toAdd.prev = head;
            tail = toAdd;
            head.next = toAdd;
        } else {
            // Insert the node after the tail and adjust references.
            toAdd.prev = tail;
            tail.next = toAdd;
            tail = tail.next;
        }
    }



    /**
     * Removes a node from the front of the list.
     */
    public void removeFront() {
        if (head.next == null)
            return;
        removeNode(head.next);
    }



    /**
     * Removes a node from the back of the list.
     */
    public void removeBack() {
        if (tail == null)
            return;
        removeNode(tail);
    }



    /**
     * Finds all instances of the given argument and removes them from the list.
     * 
     * @param toRemove
     *            the data to find and remove along with each node that carries
     *            it.
     * @return true if there was deletion; false if not.
     */
    public boolean removeAllInstancesOf(T toRemove) {
        boolean removedNodes = false;
        // List is empty.
        if (head.next == null)
            return removedNodes;
        // Create a pointer Node to evaluate each node. Set it to the first
        // node.
        Node ptr = head.next;
        while (ptr != null) {
            // If the node we are examining needs to be deleted.
            if (ptr.data.equals(toRemove)) {
                removeNode(ptr);
                removedNodes = true;
            }
            // Advance the iterator.
            ptr = ptr.next;
        }
        return removedNodes;
    }



    /**
     * Private helper method that will remove a node and adjust references.
     * 
     * @param toRemove
     */
    private void removeNode(Node toRemove) {
        // If we are removing a tail node.
        if (toRemove.next == null) {
            toRemove.prev.next = null;
            tail = toRemove.prev;
        }
        // If we are removing the first node in the list.
        else {
            toRemove.prev.next = toRemove.next;
            toRemove.next.prev = toRemove.prev;
        }
    }



    /**
     * Gets the data from the node at the front of the list, after the head
     * node.
     */
    public T getFirst() {
        // List was empty.
        if (head.next == null)
            return null;
        // Return node data in front of head.
        return head.next.data;
    }



    /**
     * Gets the data from the node at the end of the list, at the tail node.
     */
    public T getLast() {
        // List was empty.
        if (tail == null)
            return null;
        // Return tail node data.
        return tail.data;
    }



    /**
     * Clears the entire LinkedList.
     */
    public void clear() {
        // Sets the head and tail anew. Garbage collector will get rid of the
        // rest.
        head = new Node();
        tail = null;
    }



    /**
     * Prints the list going from head to tail.
     */
    public void printListForward() {
        Node ptr = head;
        // Iterates through the LinkedList from head to tail, printing each one
        // as
        // it passes by it.
        while (ptr.next != null) {
            ptr = ptr.next;
            System.out.print(ptr.data);
            if (ptr.next != null)
                System.out.print(", ");
        }
        System.out.println();
    }



    /**
     * Prints the list going from tail to head.
     */
    public void printListReverse() {
        Node ptr = tail;
        if (tail == null)
            return;
        // Iterates through the LinkedList from tail to head-1, printing each
        // one
        // as
        // it passes by it.
        while (ptr.data != null) {
            System.out.print(ptr.data);
            ptr = ptr.prev;
            if (ptr.data != null)
                System.out.print(", ");
        }
        System.out.println();
    }

    /**
     * The Linked List Node will store objects and will be the basis of our
     * LinkedList.
     * 
     * @author Toni N. Tran
     * 
     */
    class Node {

        Node prev, next;
        T data;



        /**
         * Default constructor.
         */
        public Node() {
            data = null;
        }



        /**
         * Parameterized constructor that wraps the argument in a node.
         * 
         * @param toWrap
         */
        public Node(T toWrap) {
            data = toWrap;
        }



        /**
         * toString() for debugging purposes.
         */
        public String toString() {
            return data.toString();
        }
    }

}
